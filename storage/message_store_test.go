package storage

import (
	"cwtch.im/cwtch/protocol"
	"os"
	"strconv"
	"testing"
)

func TestMessageStore(t *testing.T) {
	os.Remove("ms.test")
	ms := new(MessageStore)
	ms.Init("ms.test", 100000)
	for i := 0; i < 50000; i++ {
		gm := protocol.GroupMessage{
			Ciphertext: []byte("Hello this is a fairly average length message that we are writing here. " + strconv.Itoa(i)),
			Spamguard:  []byte{},
		}
		ms.AddMessage(gm)
	}
	ms.Close()
	ms.Init("ms.test", 100000)
	m := ms.FetchMessages()
	if len(m) != 50000 {
		t.Errorf("Should have been 5000 was %v", len(m))
	}

	for i := 0; i < 100000; i++ {
		gm := protocol.GroupMessage{
			Ciphertext: []byte("Hello this is a fairly average length message that we are writing here. " + strconv.Itoa(i)),
			Spamguard:  []byte{},
		}
		ms.AddMessage(gm)
	}

	m = ms.FetchMessages()
	if len(m) != 100000 {
		t.Errorf("Should have been 100000 was %v", len(m))
	}

	ms.Close()
	os.Remove("ms.test")
}
