package main

import (
	cwtchserver "cwtch.im/cwtch/server"
	"github.com/s-rah/go-ricochet/utils"
	"io/ioutil"
	"log"
	"os"
)

const privateKeyFile = "./private_key"

func checkAndGenPrivateKey(privateKeyFile string) {
	if _, err := os.Stat(privateKeyFile); os.IsNotExist(err) {
		log.Printf("no private key found!")
		log.Printf("generating new private key...")
		pk, err := utils.GeneratePrivateKey()
		if err != nil {
			log.Fatalf("error generating new private key: %v\n", err)
		}
		err = ioutil.WriteFile(privateKeyFile, []byte(utils.PrivateKeyToString(pk)), 0400)
		if err != nil {
			log.Fatalf("error writing new private key to file %s: %v\n", privateKeyFile, err)
		}
	}
}

func main() {
	checkAndGenPrivateKey(privateKeyFile)

	server := new(cwtchserver.Server)
	log.Printf("starting cwtch server...")

	// TODO load params from .cwtch/server.conf or command line flag
	server.Run(privateKeyFile, 100000)
}
