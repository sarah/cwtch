package server

import (
	"cwtch.im/cwtch/server/fetch"
	"cwtch.im/cwtch/server/listen"
	"cwtch.im/cwtch/server/send"
	"cwtch.im/cwtch/storage"
	"github.com/s-rah/go-ricochet/application"
	"github.com/s-rah/go-ricochet/channels"
	"github.com/s-rah/go-ricochet/utils"
	"log"
)

// Server encapsulates a complete, compliant Cwtch server.
type Server struct {
	app *application.RicochetApplication
}

// Run starts a server with the given privateKey
// TODO: surface errors
func (s *Server) Run(privateKeyFile string, bufferSize int) {
	cwtchserver := new(application.RicochetApplication)

	pk, err := utils.LoadPrivateKeyFromFile(privateKeyFile)

	if err != nil {
		log.Fatalf("error reading private key file: %v", err)
	}

	l, err := application.SetupOnion("127.0.0.1:9051", "tcp4", "", pk, 9878)

	if err != nil {
		log.Fatalf("error setting up onion service: %v", err)
	}

	af := application.ApplicationInstanceFactory{}
	af.Init()
	ms := new(storage.MessageStore)
	ms.Init("cwtch.messages", bufferSize)
	af.AddHandler("im.cwtch.server.listen", func(rai *application.ApplicationInstance) func() channels.Handler {
		si := new(Instance)
		si.Init(rai, cwtchserver, ms)
		return func() channels.Handler {
			cslc := new(listen.CwtchServerListenChannel)
			return cslc
		}
	})

	af.AddHandler("im.cwtch.server.fetch", func(rai *application.ApplicationInstance) func() channels.Handler {
		si := new(Instance)
		si.Init(rai, cwtchserver, ms)
		return func() channels.Handler {
			cssc := new(fetch.CwtchServerFetchChannel)
			cssc.Handler = si
			return cssc
		}
	})

	af.AddHandler("im.cwtch.server.send", func(rai *application.ApplicationInstance) func() channels.Handler {
		si := new(Instance)
		si.Init(rai, cwtchserver, ms)
		return func() channels.Handler {
			cssc := new(send.CwtchServerSendChannel)
			cssc.Handler = si
			return cssc
		}
	})

	cwtchserver.Init("cwtch server for "+l.Addr().String()[0:16], pk, af, new(application.AcceptAllContactManager))
	log.Printf("cwtch server running on cwtch:%s", l.Addr().String()[0:16])
	s.app = cwtchserver
	cwtchserver.Run(l)
}

func (s *Server) Shutdown() {
	s.app.Shutdown()
}
