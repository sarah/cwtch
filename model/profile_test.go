package model

import (
	"cwtch.im/cwtch/protocol"
	"github.com/golang/protobuf/proto"
	"testing"
)

func TestProfile(t *testing.T) {
	profile := GenerateNewProfile("Sarah")
	err := profile.Save("./profile_test")
	if err != nil {
		t.Errorf("Should have saved profile, but got error: %v", err)
	}
	loadedProfile, err := LoadProfile("./profile_test")
	if err != nil || loadedProfile.Name != "Sarah" {
		t.Errorf("Issue loading profile from file %v %v", err, loadedProfile)
	}
}

func TestProfileIdentity(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")

	message := sarah.GetCwtchIdentityPacket()

	ci := &protocol.CwtchPeerPacket{}
	err := proto.Unmarshal(message, ci)
	if err != nil {
		t.Errorf("alice should have added sarah as a contact %v", err)
	}
	alice.AddCwtchIdentity("sarah.onion", ci.GetCwtchIdentify())
	if alice.Contacts["sarah.onion"].Name != "Sarah" {
		t.Errorf("alice should have added sarah as a contact %v", alice.Contacts)
	}

	t.Logf("%v", alice)
}

func TestTrustPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.TrustPeer(sarah.Onion)
	if alice.IsBlocked(sarah.Onion) {
		t.Errorf("peer should not be blocked")
	}

	if alice.TrustPeer("") == nil {
		t.Errorf("trusting a non existent peer should error")
	}
}

func TestBlockPeer(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)
	alice.BlockPeer(sarah.Onion)
	if !alice.IsBlocked(sarah.Onion) {
		t.Errorf("peer should not be blocked")
	}

	if alice.BlockPeer("") == nil {
		t.Errorf("blocking a non existent peer should error")
	}
}

func TestAcceptNonExistentGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	sarah.AcceptInvite("doesnotexist")
}

func TestRejectGroupInvite(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("aaa.onion")
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)
	group := alice.GetGroupByGroupID(gid)
	if len(sarah.Groups) == 1 {
		if sarah.GetGroupByGroupID(group.GroupID).Accepted {
			t.Errorf("Group should not be accepted")
		}
		sarah.RejectInvite(group.GroupID)
		if len(sarah.Groups) != 0 {
			t.Errorf("Group %v should have been deleted", group.GroupID)
		}
		return
	}
	t.Errorf("Group should exist in map")
}

func TestProfileGroup(t *testing.T) {
	sarah := GenerateNewProfile("Sarah")
	alice := GenerateNewProfile("Alice")
	sarah.AddContact(alice.Onion, &alice.PublicProfile)
	alice.AddContact(sarah.Onion, &sarah.PublicProfile)

	gid, invite, _ := alice.StartGroup("aaa.onion")
	gci := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite, gci)
	sarah.ProcessInvite(gci.GetGroupChatInvite(), alice.Onion)
	group := alice.GetGroupByGroupID(gid)
	sarah.AcceptInvite(group.GroupID)
	c, _ := sarah.EncryptMessageToGroup("Hello World", group.GroupID)
	alice.AttemptDecryption(c)

	gid2, invite2, _ := alice.StartGroup("bbb.onion")
	gci2 := &protocol.CwtchPeerPacket{}
	proto.Unmarshal(invite2, gci2)
	sarah.ProcessInvite(gci2.GetGroupChatInvite(), alice.Onion)
	group2 := alice.GetGroupByGroupID(gid2)
	c2, _ := sarah.EncryptMessageToGroup("Hello World", group2.GroupID)
	alice.AttemptDecryption(c2)

	bob := GenerateNewProfile("bob")
	bob.AddContact(alice.Onion, &alice.PublicProfile)
	bob.ProcessInvite(gci2.GetGroupChatInvite(), alice.Onion)
	c3, err := bob.EncryptMessageToGroup("Bobs Message", group2.GroupID)
	if err == nil {
		ok, message := alice.AttemptDecryption(c3)
		if ok != true || message.Verified == true {
			t.Errorf("Bobs message to the group should be decrypted but not verified by alice instead %v %v", message, ok)
		}

		eve := GenerateNewProfile("eve")
		ok, _ = eve.AttemptDecryption(c3)
		if ok {
			t.Errorf("Eves hould not be able to decrypt messages!")
		}
	} else {
		t.Errorf("Bob failed to encrypt a message to the group")
	}
}
