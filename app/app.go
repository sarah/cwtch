package app

import (
	"cwtch.im/cwtch/peer"
	"log"
)

// Application is a facade over a CwtchPeer that provides some wrapping logic.
type Application struct {
	Peer *peer.CwtchPeer
}

// NewProfile creates a new CwtchPeer with a given name.
func (app *Application) NewProfile(name string, filename string) error {
	profile := peer.NewCwtchPeer(name)
	app.Peer = profile
	err := profile.Save(filename)
	if err == nil {
		go func() {
			err := app.Peer.Listen()
			if err != nil {
				log.Panic(err)
			}
		}()
	}
	return err
}

// SetProfile loads an existing profile from the given filename.
func (app *Application) SetProfile(filename string) error {
	profile, err := peer.LoadCwtchPeer(filename)
	app.Peer = profile
	if err == nil {
		go func() {
			err := app.Peer.Listen()
			if err != nil {
				log.Panic(err)
			}
		}()
	}
	return err
}

// PeerRequest attempts to setup peer relationship with the given onion address.`
func (app *Application) PeerRequest(onion string) {
	app.Peer.PeerWithOnion(onion)
}
