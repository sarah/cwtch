package peer

import (
	"testing"
)

func TestCwtchPeerGenerate(t *testing.T) {

	alice := NewCwtchPeer("alice")
	alice.Save("./test_profile")

	aliceLoaded, err := LoadCwtchPeer("./test_profile")
	if err != nil || aliceLoaded.Profile.Name != "alice" {
		t.Errorf("something went wrong saving and loading profiles %v %v", err, aliceLoaded)
	}

	groupID, _, _ := aliceLoaded.Profile.StartGroup("test.server")
	exportedGroup, _ := aliceLoaded.ExportGroup(groupID)
	t.Logf("Exported Group: %v from %v", exportedGroup, aliceLoaded.Profile.Onion)

	importedGroupID, err := alice.ImportGroup(exportedGroup)
	group := alice.Profile.GetGroupByGroupID(importedGroupID)
	t.Logf("Imported Group: %v, err := %v %v", group, err, importedGroupID)

}
